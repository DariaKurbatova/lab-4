import java.util.Scanner;
public class Shop {
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		Dress[] dresses = new Dress[4];
		for(int i=0; i < 4; i++){
			System.out.println("Enter the price of the dress: ");
			double priceInput = Integer.parseInt(keyboard.nextLine());
			System.out.println("Enter the color of the dress: ");
			String colorInput = keyboard.nextLine();
			System.out.println("Enter the length of the dress: ");
			String lengthInput = keyboard.nextLine();
			dresses[i] = new Dress(priceInput, colorInput, lengthInput);
		}
		System.out.println("The last dress is:  "+dresses[3].getPrice()+"$, it is "+dresses[3].getColor()+", and it is "+dresses[3].getLength());
		
		dresses[3].discount();
		System.out.println("After the discount, the last dress is:  "+dresses[3].getPrice()+"$, it is "+dresses[3].getColor()+", and it is "+dresses[3].getLength());
		
		System.out.println("Enter new values for the last dress:");
		System.out.println("Enter the price of the dress: ");
		double price = Integer.parseInt(keyboard.nextLine());
		System.out.println("Enter the color of the dress: ");
		String color = keyboard.nextLine();
		System.out.println("Enter the length of the dress: ");
		String length = keyboard.nextLine();
		System.out.println("before the call of the setters, the values are:");
		System.out.println("The last dress is:  "+dresses[3].getPrice()+"$, it is "+dresses[3].getColor()+", and it is "+dresses[3].getLength());
		
		dresses[3].setPrice(price);
		dresses[3].setColor(color);
		dresses[3].setLength(length);
		System.out.println("after the call of the setters, the values are:");
		System.out.println("The last dress is:  "+dresses[3].getPrice()+"$, it is "+dresses[3].getColor()+", and it is "+dresses[3].getLength());
	}
}

